import java.util.Scanner;

public class Produtora {

	public static void main(String[] args) {
		int opcao = -1;
		Scanner sc = new Scanner(System.in);
		Artista artista = new Artista();
		EmpresaEventos empresa = new EmpresaEventos();
		
		while (opcao != 0) {
			System.out.println("");
			System.out.println("Menu: ");
			System.out.println("1- Adicionar T�tulo");
			System.out.println("2- Excluir T�tulo");
			System.out.println("3- Buscar T�tulo");
			System.out.println("4- Contratar Show");
			System.out.println("0- Sair");
			System.out.println("");
			System.out.print("Op��o: ");
			opcao = sc.nextInt();
			
			switch (opcao) {
				case 1:
					Titulo titulo = new Titulo();
					System.out.print("Informe o g�nero: ");
					titulo.setGenero(sc.next());
					System.out.print("Informe a descri��o: ");
					titulo.setDescricao(sc.next());
					System.out.print("Informe o ano de lan�amento: ");
					titulo.setAnoLanc(sc.nextInt());
					
					if (artista.lancarTitulo(titulo)) {
						System.out.println("T�tulo lan�ado com sucesso");
					} else {
						System.out.println("Falha ao lan�ar t�tulo");
					}
				break;
				
				case 2:
					System.out.println("T�tulos");
					for (Titulo row : artista.getListaTitulo()) {
						System.out.println("[" + row.getCodigo() + "] " + row.getDescricao());
					}
					System.out.print("Informe o c�digo do t�tulo: ");
					if (artista.excluirTitulo(sc.nextInt())) {
						System.out.println("T�tulo exclu�do com sucesso");
					} else {
						System.out.println("Falha ao remover t�tulo");
					}
					
				break;
				
				case 3:
					System.out.print("Informe o c�digo do t�tulo: ");
					Titulo tituloEncontrado = artista.buscarTitulo(sc.nextInt());
					if (tituloEncontrado.getCodigo() > 0) {
						System.out.println("C�digo: " + tituloEncontrado.getCodigo());
						System.out.println("G�nero: " + tituloEncontrado.getGenero());
						System.out.println("Descri��o: " + tituloEncontrado.getDescricao());
						System.out.println("Ano de lan�amento: " + tituloEncontrado.getAnoLanc());
					} else {
						System.out.println("T�tulo n�o encontrado");
					}
				break;
				
				case 4:
					Show show = new Show();
					Artista artistaShow = new Artista();
					
					System.out.print("Informe o c�digo do show: ");
					show.setCodigo(sc.nextInt());
					System.out.print("Informe o m�s do show: ");
					show.setCodigo(sc.nextInt());
					System.out.print("Informe o c�digo do artista: ");
					artistaShow.setCodigo(sc.nextInt());
					System.out.print("Informe o nome do artista: ");
					artistaShow.setNome(sc.next());
					System.out.print("Informe a nacionalidade do artista: ");
					artistaShow.setNacionalidade(sc.next());
					System.out.print("Informe o valor do show do artista: ");
					artistaShow.setValorShow(sc.nextDouble());
					show.setArtista(artistaShow);
					
					if (empresa.contratarShow(show)) {
						System.out.println("Show contratado com sucesso");
					} else {
						System.out.println("Falha ao contratar show");
					}
					
				break;
				
				default:
					if (opcao != 0) {
						opcao = -1;
						System.out.println("Informe uma op��o v�lida!");
					}	
					break;
			}
		}
	}

}
