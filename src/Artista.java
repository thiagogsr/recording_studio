import java.util.ArrayList;

public class Artista {
	
	private int codigo;
	private String nome;
	private double valorShow;
	private String nacionalidade;
	private ArrayList<Titulo> listaTitulo;
	private byte limiteTitulos;
	
	public Artista() {
		this.limiteTitulos = 10;
		this.listaTitulo = new ArrayList<Titulo>(this.limiteTitulos);
	}
	
	public Artista(String nome, double valorShow, String nacionalidade) {
		this.nome = nome;
		this.valorShow = valorShow;
		this.nacionalidade = nacionalidade;
	}
	
	public boolean lancarTitulo(Titulo novoTitulo) {
		// valida se ainda h� espa�o na lista
		if (this.listaTitulo.size() == this.limiteTitulos) {
			return false;
		}
		
		// valida se h� 2 t�tulos no mesmo ano
		int ano = novoTitulo.getAnoLanc();
		byte count = 0;
		boolean hasTwo = false;
		
		for (Titulo titulo : this.listaTitulo) {
			if (titulo.getAnoLanc() == ano) {
				count++;
			}
			if (count == 2) {
				hasTwo = true;
				break;
			}
		}
		
		if (hasTwo) {
			return false;
		}
		
		novoTitulo.setCodigo(this.proximoCodigoTitulo());
		return this.listaTitulo.add(novoTitulo);
	}
	
	public boolean excluirTitulo(int codigo) {
		return this.buscarTitulo(codigo).getCodigo() > 0 ? this.listaTitulo.remove(this.buscarTitulo(codigo)) : false;
	}
	
	public Titulo buscarTitulo(int codigo) {
		Titulo titulo = new Titulo();
		
		for (Titulo row : this.listaTitulo) {
			if (row.getCodigo() == codigo) {
				titulo = row;
				break;
			}
		}
		
		return titulo;
	}
	
	public int proximoCodigoTitulo() {
		return this.listaTitulo.size() > 0 ? this.listaTitulo.get(this.listaTitulo.size() - 1).getCodigo() + 1 : 1;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getValorShow() {
		return valorShow;
	}
	public void setValorShow(double valorShow) {
		this.valorShow = valorShow;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public ArrayList<Titulo> getListaTitulo() {
		return listaTitulo;
	}
	public void setListaTitulo(ArrayList<Titulo> listaTitulo) {
		this.listaTitulo = listaTitulo;
	}

}
