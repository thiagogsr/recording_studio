import java.util.ArrayList;

public class EmpresaEventos {
	
	private int cnpj;
	private String nome;
	private ArrayList<Show> listaShow;
	private int qtdMaxShow;
	
	public EmpresaEventos() {
		this.listaShow = new ArrayList<Show>(qtdMaxShow);
	}
	
	public EmpresaEventos(int cnpj, String nome, int qtdMaxShow) {
		this.cnpj = cnpj;
		this.nome = nome;
		this.qtdMaxShow = qtdMaxShow;
	}
	
	public boolean contratarShow(Show show) {
		// valida se ainda h� espa�o na lista
		if (this.listaShow.size() == this.qtdMaxShow) {
			return false;
		}
		
		// valida se o codigo ja existe
		boolean encontrou = false;
		for (Show row : this.listaShow) {
			if (row.getCodigo() == show.getCodigo()) {
				encontrou = true;
				break;
			}
		}
		
		if (encontrou) {
			return false;
		}
		
		return listaShow.add(show);
	}
	
	public int getCnpj() {
		return cnpj;
	}
	public void setCnpj(int cnpj) {
		this.cnpj = cnpj;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public ArrayList<Show> getListaShow() {
		return listaShow;
	}
	public void setListaShow(ArrayList<Show> listaShow) {
		this.listaShow = listaShow;
	}

}
