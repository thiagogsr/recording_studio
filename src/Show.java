public class Show {
	
	private int codigo;
	private String mes;
	private Artista artista;
	
	public Show() {}
	
	public Show(int codigo, String mes, Artista artista) {
		this.codigo = codigo;
		this.mes = mes;
		this.artista = artista;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public Artista getArtista() {
		return artista;
	}
	public void setArtista(Artista artista) {
		this.artista = artista;
	}

}
