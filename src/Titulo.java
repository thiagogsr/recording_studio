public class Titulo {
	
	private int codigo;
	private String genero;
	private String descricao;
	private int anoLanc;
	
	public Titulo() {}
	
	public Titulo(String genero, String descricao, int anoLanc) {
		this.genero = genero;
		this.descricao = descricao;
		this.anoLanc = anoLanc;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getAnoLanc() {
		return anoLanc;
	}
	public void setAnoLanc(int anoLanc) {
		this.anoLanc = anoLanc;
	}

}
